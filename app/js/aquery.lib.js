/**
 * Представляет класс AQuery для работы с элементами DOM
 */
class AQuery {
	/**
	 * Создать новый экземпляр AQuery
	 * @param {string|HTMLElement|Document} selector Строка селектора CSS или HTMLElement
	 */
	constructor(selector) {
		if( typeof selector === 'string' ) {
			this.element = document.querySelector(selector)

			if (!this.element) {
				throw new Error(`Element ${selector} not found!`)
			}
		} else if ( selector instanceof HTMLElement || selector instanceof Document) {
			this.element = selector
		} else {
			throw new Error(`Invalid selector type!`)
		}
	}

	/* FIND */

	/**
	 * Найдите первый элемент, соответствующий указанному селектору внутри выбранного элемента
	 * @param {string} selector Строка селектора CSS для поиска внутри выбранного элемента
	 * @returns {AQuery} Новый экземпляр AQuery для найденного элемента
	 */
	find(selector) {
		const element = new AQuery(this.element.querySelector(selector))

		if (element) {
			return element
		} else {
			throw new Error(`Element ${selector} not found!`)
		}
	}

	/**
	 * Найти все элементы, соответствующие указанному селектору, внутри выбранного элемента.
	 * @param {string} selector Строка селектора CSS для поиска внутри выбранного элемента.
	 * @returns {AQuery[] }Массив новых экземпляров AQuery для найденных элементов.
	 */
	findAll(selector) {
		const elements = this.element.querySelectorAll(selector)
		return Array.from(elements).map(element => new AQuery(element))
	}

	/* INSERT */

	/**
	 * Добавить новый элемент в качестве дочернего элемента выбранного элемента
	 * @param {HTMLElement} childElement Новый дочерний элемент для добавления
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	append(childElement) {
		this.element.appendChild(childElement)
		return this
	}

	/**
	 * Вставить новый элемент перед выбранным элементом
	 * @param {HTMLElement} newElement Новый элемент, который нужно вставить перед выбранным элементом.
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	before(newElement) {
		if (!(newElement instanceof HTMLElement)) {
			throw new Error('Element must be an HTMLElement')
		}

		const parentElement = this.element.parentElement

		if (parentElement) {
			parentElement.insertBefore(newElement, this.element)
			return this
		} else {
			throw new Error('Element does not have a parent element')
		}
	}

	/**
	 * Получить или установить внутренний HTML выбранного элемента
	 * @param {string} [htmlContent] Необязательный HTML-контент для установки. Если не указано, будет возвращен текущий внутренний HTML
	 * @returns {AQuery|string} Текущий экземпляр AQuery для цепочки при настройке содержимого HTML или текущий внутренний HTML при получении
	 */
	html(htmlContent) {
		if (typeof htmlContent === 'undefined') {
			return this.element.innerHTML
		} else {
			this.element.innerHTML = htmlContent
			return this
		}
	}

	/**
	 * Получить или установить внутренний текст выбранного элемента
	 * @param {string} [textContent] Необязательный текст для установки. Если не указано, будет возвращен текущий внутренний текст
	 * @returns {AQuery|string} Текущий экземпляр AQuery для цепочки при настройке содержимого текста или текущий внутренний текст при получении
	 */
	text(textContent) {
		if (typeof textContent === 'undefined') {
			return this.element.textContent
		} else {
			this.element.textContent = textContent
			return this
		}
	}

	/* EVENTS */

	/**
	 * Добавьте прослушиватель событий к выбранному элементу для указанного типа событий
	 * @param {string} eventType - Тип события, которое необходимо прослушивать (например, "click", "input" ...)
	 * @param {function(Event): void} callback - Функция прослушивателя событий, выполняемая при возникновении события. Функция получит объект события в качестве аргумента
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	on(eventType, callback) {
		if (typeof eventType !== 'string' || typeof callback !== 'function') {
			throw new Error(
				'eventType must be a string and callback must be a function'
			)
		}

		this.element.addEventListener(eventType, callback)
		return this
	}

	/**
	 * Прикрепите прослушиватель событий клика к выбранному элементу
	 * @param {function(Event): void} callback Функция прослушивателя событий, выполняемая при нажатии выбранного элемента. Функция получит объект события в качестве аргумента
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	click(callback) {
		this.element.addEventListener('click', callback)
		return this
	}

	ready(callback) {
		this.element.addEventListener('DOMContentLoaded', callback)
		return this
	}

	/* FORM */

	/**
	 * Получает или задает значение входного элемента
	 * @param {string} [newValue] Новое значение, которое нужно установить для элемента ввода. Если он не указан, метод возвращает текущее значение
	 * @return {string|AQuery} Если указано newValue, возвращает экземпляр AQuery. В противном случае возвращает текущее значение входного элемента
	 */
	value(newValue) {
		if (typeof newValue === 'undefined') {
			return this.element.value
		} else {
			this.element.value = newValue
			return this
		}
	}

	/**
	 * Установите прослушиватель событий для события отправки элемента формы
	 * @param {function(Event): void} onSubmit Прослушиватель событий отправки формы
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	submit(onSubmit) {
		if (this.element.tagName.toLowerCase() === 'form') {
			this.element.addEventListener('submit', e => {
				e.preventDefault()
				onSubmit(e)
			})
		} else {
			throw new Error('Element must be a form')
		}

		return this
	}

	/**
	 * Установите атрибуты и прослушиватели событий для элемента ввода
	 * @param {object} options Объект, содержащий параметры ввода
	 * @param {function(Event): void} [options.onInput] Прослушиватель событий для входного события ввода
	 * @param {object} [options.rest] Необязательные атрибуты, которые можно установить для элемента ввода
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	input({ onInput, ...rest }) {
		if (this.element.tagName.toLowerCase() !== 'input')
			throw new Error('Element must be an input')

		for (const [key, value] of Object.entries(rest)) {
			this.element.setAttribute(key, value)
		}

		if (onInput) {
			this.element.addEventListener('input', onInput)
		}

		return this
	}

	/**
	 * Установите атрибуты и прослушиватели событий для элемента ввода числа
	 * @param {number} [limit] Максимальная длина входного значения
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	numberInput(limit) {
		if (
			this.element.tagName.toLowerCase() !== 'input' ||
			this.element.type !== 'number'
		)
			throw new Error('Element must be an input with type "number"')

		this.element.addEventListener('input', event => {
			let value = event.target.value.replace(/[^0-9]/g, '')
			if (limit) value = value.substring(0, limit)
			event.target.value = value
		})

		return this
	}

	/* STYLES */

	/**
	 * Показывает выбранный элемент, удаляя свойство стиля display
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	show() {
		this.element.style.removeProperty('display')
		return this
	}

	/**
	 * Скрывает выбранный элемент, устанавливая для него стиль отображения "none".
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	hide() {
		this.element.style.display = 'none'
		return this
	}

	/**
	 * Установите стиль CSS выбранного элемента
	 * @param {string} property Свойство CSS, которое нужно установить
	 * @param {string} value Значение, которое нужно установить для свойства CSS
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	css(property, value) {
		if (typeof property !== 'string' || typeof value !== 'string') {
			throw new Error(`Property and Value must be strings`)
		}

		this.element.style[property] = value
		return this
	}

	/**
	 * Добавляет класс или список классов к текущему элементу
	 * @param {string|string[]} classNames Имя класса или массив имен классов для добавления к элементу
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	addClass(classNames) {
		if (Array.isArray(classNames)) {
			for (const className of classNames) {
				this.element.classList.add(className)
			}
		} else this.element.classList.add(classNames)
		return this
	}

	/**
	 * Удаляет класс или список классов у текущего элемента
	 * @param {string|string[]} classNames Имя класса или массив имен классов для удаления у элемента
	 * @returns {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	removeClass(classNames) {
		if (Array.isArray(classNames)) {
			for (const className of classNames) {
				this.element.classList.remove(className)
			}
		} else this.element.classList.remove(classNames)
		return this
	}

	/**
	 * Установите или получите значение атрибута выбранного элемента
	 * @param {string} attributeName Имя атрибута, который нужно установить или получить
	 * @param {string} [value] Значение, которое необходимо установить для атрибута. Если не указано, будет возвращено текущее значение атрибута
	 * @returns {AQuery|string} Текущий экземпляр AQuery для связывания (если задано) или значение атрибута (если получено)
	 */
	attr(attributeName, value) {
		if (typeof attributeName !== 'string') {
			throw new Error('Attribute name must be a string')
		}

		if (typeof value === 'undefined') {
			return this.element.getAttribute(attributeName)
		} else {
			this.element.setAttribute(attributeName, value)
			return this
		}
	}

	/**
	 * Удаляет атрибут из текущего элемента
	 * @param {string} attrName Имя атрибута, который нужно удалить
	 * @return {AQuery} Текущий экземпляр AQuery для цепочки
	 */
	removeAttr(attrName) {
		if (typeof attrName !== 'string') {
			throw new Error('attrName must be a string')
		}

		this.element.removeAttribute(attrName)
		return this
	}
}

/**
 * Создайте новый экземпляр AQuery для данного селектора
 * @param {string|HTMLElement|Document} selector Строка селектора CSS или HTMLElement
 * @returns {AQuery} Новый экземпляр AQuery для данного селектора
 */
export default function $A(selector) {
	return new AQuery(selector)
}
